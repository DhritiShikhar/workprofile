### As a speaker/ volunteer

| Event | Role |  |
| ------ | ------ | ---|
| Golang Bangalore Meetup, India | Co-organizer of one of Asia's largest Golang meetup group | June 2017 - September 2018 |
| Red Hat India Pvt. Ltd., Bangalore, India | Conducted Golang workshop | September 2018, September 2019 |
| Red Hat Summit At Sites, Bangalore, India | Presented a talk on User Defined Order in PostgreSQL | May 2018 |
| GopherCon India, Pune, India | As one of the members in panel discussion | February 2018 |
| PyCon India, Bangalore | Mentor for Pagure project; Poster presentation participant; Conference volunteer | 2015 |
| Pune Python Meetup, India | Participant; Speaker; Presented a talk on getting started with Fedora-Infra | 2015 |
| PyLadies, Pune, India | Delivered a workshop on Git | 2014 |
| PyCon India, Bangalore | Conference volunteer | 2014 |